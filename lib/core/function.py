from  __future__ import  absolute_import
import time
import lib.utils.utils as utils
import torch
from torch.autograd import Variable

# import torch_xla.core.xla_model as xm

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        if isinstance(val, Variable):
            count = val.data.numel()
            val = val.data.sum()
        elif isinstance(val, torch.Tensor):
            count = val.numel()
            val = val.sum()

        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


def train(config, train_loader, dataset, converter, model, criterion, image, text, length, optimizer, device, epoch, writer_dict=None, output_dict=None):

    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()

    for p in model.parameters():
        p.requires_grad = True

    train_len = len(train_loader)

    model.train()

    end = time.time()
    for i, (inp, idx) in enumerate(train_loader):
        # measure data time
        data_time.update(time.time() - end)

        labels = utils.get_batch_label(dataset, idx)

        batch_size = inp.size(0)
        utils.load_data(image, inp)

        # inference
        preds = model(image)

        # compute loss
        t, l = converter.encode(labels)                    # length = 一个batch中的总字符长度, text = 一个batch中的字符所对应的下标
        utils.load_data(text, t)
        utils.load_data(length, l)

        # preds_size = torch.IntTensor([preds.size(0)] * batch_size) # timestep * batchsize
        # loss = criterion(preds, text, preds_size, length)
        preds_size = Variable(torch.IntTensor([preds.size(0)] * batch_size))
        loss = criterion(preds, text, preds_size, length)

        optimizer.zero_grad()
        loss.backward()

        # if config.USE_XLA:
        #   xm.optimizer_step(optimizer)
        #   xm.mark_step()
        # else:
        optimizer.step()

        losses.update(loss.item(), batch_size)

        batch_time.update(time.time()-end)
        if i % config.PRINT_FREQ == 0:
            msg = 'Epoch: [{0}][{1}/{2}]\t' \
                  'ETA {eta:.3f}h ({eta_avg:.3f}h)\t' \
                  'Time {batch_time.val:.3f}s ({batch_time.avg:.3f}s)\t' \
                  'Speed {speed:.1f} samples/s\t' \
                  'Data {data_time.val:.3f}s ({data_time.avg:.3f}s)\t' \
                  'Loss {loss.val:.5f} ({loss.avg:.5f})\t'.format(
                      epoch, i, train_len, batch_time=batch_time,
                      eta=(batch_time.val*(train_len-i))/60/60,
                      eta_avg=(batch_time.avg*(train_len-i))/60/60,
                      speed=batch_size/batch_time.val,
                      data_time=data_time, loss=losses)
            print(msg)

            if writer_dict:
                writer = writer_dict['writer']
                global_steps = writer_dict['train_global_steps']
                writer.add_scalar('train_loss', losses.avg, global_steps)
                writer_dict['train_global_steps'] = global_steps + 1

        end = time.time()


def validate(config, val_loader, dataset, converter, model, criterion, image, text, length, device, epoch, writer_dict, output_dict):

    losses = AverageMeter()
    model.eval()

    n_correct = 0
    with torch.no_grad():
        for i, (inp, idx) in enumerate(val_loader):

            labels = utils.get_batch_label(dataset, idx)

            batch_size = inp.size(0)
            utils.load_data(image, inp)

            # inference
            preds = model(image)

            # compute loss
            t, l = converter.encode(labels)
            utils.load_data(text, t)
            utils.load_data(length, l)

            # preds_size = torch.IntTensor([preds.size(0)] * batch_size)
            # loss = criterion(preds, text, preds_size, length)
            preds_size = Variable(torch.IntTensor([preds.size(0)] * batch_size))
            loss = criterion(preds, text, preds_size, length)

            losses.update(loss.item(), batch_size)

            _, preds = preds.max(2)
            preds = preds.transpose(1, 0).contiguous().view(-1)
            sim_preds = converter.decode(preds.data, preds_size.data, raw=False)
            for pred, target in zip(sim_preds, labels):
                if pred == target:
                    n_correct += 1

            if (i + 1) % config.PRINT_FREQ == 0:
                print('Epoch: [{0}][{1}/{2}]'.format(epoch, i, len(val_loader)))

            if i == config.TEST.NUM_TEST_BATCH:
                break

    raw_preds = converter.decode(preds.data, preds_size.data, raw=True)[:config.TEST.NUM_TEST_DISP]
    for raw_pred, pred, gt in zip(raw_preds, sim_preds, labels):
        print('%-20s => %-20s, gt: %-20s' % (raw_pred, pred, gt))

    num_test_sample = config.TEST.NUM_TEST_BATCH * config.TEST.BATCH_SIZE_PER_GPU
    if num_test_sample > len(dataset):
        num_test_sample = len(dataset)

    print("[#correct:{} / #total:{}]".format(n_correct, num_test_sample))
    accuracy = n_correct / float(num_test_sample)
    print('Test loss: {:.4f}, accuray: {:.4f}'.format(losses.avg, accuracy))

    if writer_dict:
        writer = writer_dict['writer']
        global_steps = writer_dict['valid_global_steps']
        writer.add_scalar('valid_acc', accuracy, global_steps)
        writer_dict['valid_global_steps'] = global_steps + 1

    return accuracy
