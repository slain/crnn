# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import cv2
import numpy as np

def show_image(img):
  cv2.imshow('show', img)
  cv2.waitKey(0)

def crop_area(crop):
  x1, y1, x2, y2 = crop
  return max(0, x2 - x1) * max(0, y2 - y1)

def union_crops(crop1, crop2):
  """Union two (x1, y1, x2, y2) rects."""
  x11, y11, x21, y21 = crop1
  x12, y12, x22, y22 = crop2
  return min(x11, x12), min(y11, y12), max(x21, x22), max(y21, y22)

def props_for_contours(contours, ary):
  """Calculate bounding box & the number of set pixels for each contour."""
  c_info = []
  for c in contours:
    x,y,w,h = cv2.boundingRect(c)
    c_im = np.zeros(ary.shape)
    cv2.drawContours(c_im, [c], 0, 255, -1)
    c_info.append({
      'x1': x,
      'y1': y,
      'x2': x + w - 1,
      'y2': y + h - 1,
      'sum': np.sum(ary * (c_im > 0))/255
    })
  return c_info

def find_components(edges):
  """Dilate the image until there are just a few connected components.
  Returns contours for these components."""
  # Perform increasingly aggressive dilation until there are just a few
  # connected components.
  count = 21
  n = 1
  while count > 16:
    n += 1
    dilated_image = dilateX(edges, N=3, iterations=n)
    _, contours, _ = cv2.findContours(dilated_image.astype(np.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    count = len(contours)
  return contours

def find_optimal_components_subset(contours, edges):
  """Find a crop which strikes a good balance of coverage/compactness.
  Returns an (x1, y1, x2, y2) tuple.
  """
  c_info = props_for_contours(contours, edges)
  c_info.sort(key=lambda x: -x['sum'])
  total = np.sum(edges) / 255
  area = edges.shape[0] * edges.shape[1]

  c = c_info[0]
  del c_info[0]
  this_crop = c['x1'], c['y1'], c['x2'], c['y2']
  crop = this_crop
  covered_sum = c['sum']

  while covered_sum < total:
    changed = False
    recall = 1.0 * covered_sum / total
    prec = 1 - 1.0 * crop_area(crop) / area
    f1 = 2 * (prec * recall / (prec + recall))
    for i, c in enumerate(c_info):
      this_crop = c['x1'], c['y1'], c['x2'], c['y2']
      new_crop = union_crops(crop, this_crop)
      new_sum = covered_sum + c['sum']
      new_recall = 1.0 * new_sum / total
      new_prec = 1 - 1.0 * crop_area(new_crop) / area
      new_f1 = 2 * new_prec * new_recall / (new_prec + new_recall)

      remaining_frac = c['sum'] / (total - covered_sum)
      new_area_frac = 1.0 * crop_area(new_crop) / crop_area(crop) - 1
      if new_f1 > f1 or (
          remaining_frac > 0.25 and new_area_frac < 0.15):
        # print('%d %s -> %s / %s (%s), %s -> %s / %s (%s), %s -> %s' % (
        #     i, covered_sum, new_sum, total, remaining_frac,
        #     crop_area(crop), crop_area(new_crop), area, new_area_frac,
        #     f1, new_f1))
        crop = new_crop
        covered_sum = new_sum
        del c_info[i]
        changed = True
        break

    if not changed:
      break

  return crop

def threshold(img, min_value=170, max_saturation=25):
  hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
  return cv2.inRange(hsv, (0, 0, min_value), (179, max_saturation, 255))

def dilate_erode5(img):
  "Closes the img"
  kernel = np.ones((5, 5), np.uint8)
  img = cv2.dilate(img, kernel)
  img = cv2.erode(img, kernel)
  return img

def dilate_erode3(img):
  "Closes the img"
  kernel = np.ones((3, 3), np.uint8)
  img = cv2.dilate(img, kernel)
  img = cv2.erode(img, kernel)
  return img

def dilate3(img):
  kernel = np.ones((3, 3), np.uint8)
  return cv2.dilate(img, kernel)

def dilate(img, n=3):
  kernel = np.ones((n, n), np.uint8)
  return cv2.dilate(img, kernel)

def dilateX(ary, N, iterations): 
  """Dilate using an NxN '+' sign shape. ary is np.uint8."""
  kernel = np.zeros((N, N), dtype=np.uint8)
  # kernel[(N-1) / 2, :] = 1
  dilated_image = cv2.dilate(ary / 255, kernel, iterations=iterations)

  kernel = np.zeros((N, N), dtype=np.uint8)
  # kernel[:, (N-1) / 2] = 1
  dilated_image = cv2.dilate(dilated_image, kernel, iterations=iterations)
  return dilated_image

def erode(img, n=3):
  kernel = np.ones((n, n), np.uint8)
  return cv2.erode(img, kernel)

def remove_small_islands(img, min_pixels=8):
  mask = np.zeros(img.shape)
  _, contours, _ = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
  for contour in contours:
    if cv2.contourArea(contour) < min_pixels:
      cv2.fillPoly(mask, pts=contour, color=(255, 255, 255))
  return img - mask

class TextExtractor:
  def __init__(self, debug=False):
    self.debug = debug

  def extract(self, img):
    self.cleaned = self.clean_image(img)

  def crop(self, x, y, w, h):
    self.cleaned = self.cleaned[y:y + h, x:x + w]

  def clean_image(self, img):
    raise NotImplementedError

class E0(TextExtractor):
  TEXT_TOP = 960
  TEXT_BOTTOM = 960 + 70
  TEXT_LEFT = 450  # min observed was 300 pixels in, each char is 50 pixels wide
  TEXT_RIGHT = 450 + 1050  # max observed was 300 pixels in from the right

  def clean_image(self, img):
    cropped = img[
        self.TEXT_TOP: self.TEXT_BOTTOM,
        self.TEXT_LEFT: self.TEXT_RIGHT]
    self.cropped = cropped
    return self.clean_after_crop(cropped)

  def clean_after_crop(self, cropped):
    img = threshold(cropped)
    img = dilate_erode3(img)
    img = dilate3(img)
    img = img & dilate_erode5(cv2.Canny(cropped, 400, 600))
    return img

class E1(E0):
  def get_canny_mask(self, cropped):
    mask = cv2.Canny(cropped, 400, 600)
    mask = dilate(mask, 5)
    mask = erode(mask, 5)
    return mask

  def sharpen(self, img):
    blurred = cv2.GaussianBlur(img, (3, 3), 0)
    return cv2.addWeighted(img, 2, blurred, -1, 0)

  def clean_after_crop(self, cropped):
    self.sharpened = img = self.sharpen(cropped)
    if self.debug:
      show_image(self.sharpened)
    self.thresholded = img = threshold(img, min_value=191)
    if self.debug:
      show_image(self.thresholded)
    self.canny_mask = self.get_canny_mask(cropped)
    img = img & self.canny_mask
    if self.debug:
      show_image(self.canny_mask)
      show_image(img)
    img = remove_small_islands(img)
    img = dilate(img, 3)
    if self.debug:
      show_image(img)
    return img

class E2(E1):
  def get_border_floodfill_mask(self):
    mask = np.zeros(self.thresholded.shape)
    _, contours, hierarchy = cv2.findContours(self.thresholded, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_NONE)
    for root_idx, contour in enumerate(contours):
      left, top, width, height = cv2.boundingRect(contour)
      right = left + width
      bottom = top + height
      if not (top <= 4 or left <= 4
              or bottom >= self.thresholded.shape[0] - 5 or right >= self.thresholded.shape[1] - 5):
        continue

      cv2.fillPoly(mask, pts=[contour], color=(255, 255, 255))
      for child_contour, (_, _, _, parent_idx) in zip(contours, hierarchy[0]):  # TODO no idea why we have to do [0]
        if parent_idx != root_idx:
          continue
        cv2.fillPoly(mask, pts=[child_contour], color=(0, 0, 0))

    # because we do a dilate3 in super().clean_after_crop, we also need to do that here so the mask matches when we
    # subtract
    mask = dilate(mask, 3)

    return mask

  def clean_after_crop(self, cropped):
    img = super().clean_after_crop(cropped)
    self.border_floodfill_mask = self.get_border_floodfill_mask()
    # img = img - self.get_border_floodfill_mask()
    if self.debug:
      show_image(img)
    return img

class E3(E2):
  def get_border_floodfill_mask(self):
    h, w = self.thresholded.shape[:2]
    mask = np.zeros((h + 2, w + 2), np.uint8)

    border_points = []
    for r in range(5):
      for c in range(w):
        # top border
        border_points.append((r, c))
        # bottom border
        border_points.append((h - 1 - r, c))
    for c in range(5):
      for r in range(h):
        # left border
        border_points.append((r, c))
        # right border
        border_points.append((r, w - 1 - c))

    for r, c in border_points:
      if not self.thresholded[r][c]:
        continue
      # The (255 << 8) incantation means set mask value to 255 when filling. The | 8 means do an 8-neighbor fill.
      cv2.floodFill(self.thresholded, mask, (c, r), 255, flags=(255 << 8) | cv2.FLOODFILL_MASK_ONLY | 8)

    # because we do a dilate3 in super().clean_after_crop, we also need to do that here so the mask matches when we
    # subtract
    mask = dilate(mask, 3)

    return mask[1:-1, 1:-1]

class E4(E3):
  def get_canny_mask(self, cropped):
    mask = cv2.Canny(cropped, 400, 600)
    mask = dilate(mask, 5)
    mask = erode(mask, 3)
    return mask

class E5(E3):
  def sharpen(self, img):
    blurred = cv2.GaussianBlur(img, (3, 3), 0)
    return cv2.addWeighted(img, 2.7, blurred, -1.7, 0)

class B0(E0):
  """
  The first model I blogged about (in the Part 1 article).
  Pass rate: 18%.
  """

  def clean_after_crop(self, cropped):
    img = cv2.inRange(cropped, (200, 200, 200), (255, 255, 255))
    return img

class B1(B0):
  """
  Thresholding using HSV.
  Pass rate: 26%.
  """

  def clean_after_crop(self, cropped):
    return threshold(cropped, min_value=180, max_saturation=30)

class B2(B1):
  """
  Dilating the output of B1.
  Pass rate: 52%.
  """

  def clean_after_crop(self, cropped):
    return dilate(super().clean_after_crop(cropped), 3)
