# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import cv2
import textdistance
import numpy as np
import math

def timecode(ms):
  s, ms = divmod(ms, 1000)
  min, s = divmod(s, 60)
  h, min = divmod(min, 60)
  return "{:02}:{:02}:{:02},{:03}".format(h, min, s, ms)

def timecode_file(ms):
  s, ms = divmod(ms, 1000)
  min, s = divmod(s, 60)
  h, min = divmod(min, 60)
  return "{:02}-{:02}-{:02}-{:03}".format(h, min, s, ms)

def best_text(sequences):
  matrix = []
  for x, first in enumerate(sequences):
    matrix.append({ 'index': x,  'score': 0 })
    for y, second in enumerate(sequences):
      score = textdistance.levenshtein.normalized_distance(first, second)
      matrix[x]['score'] += score

  matrix.sort(key=lambda x: x['score'])

  # print(matrix)

  if len(matrix) > 0:
    best = matrix[:1][0]
    result = sequences[best['index']]
    return result
  return ''

def distance(p1, p2):
  (x1, y1) = p1
  (x2, y2) = p2
  return math.sqrt((x1 - x2)**2 + (y1 - y2)**2)

def rect_distance(r1, r2):
  (x1, y1, x1b, y1b) = r1
  (x2, y2, x2b, y2b) = r2
  left = x2b > x1
  right = x1b > x2
  bottom = y2b > y1
  top = y1b > y2
  if top and left:
    return distance((x1, y1b), (x2b, y2))
  elif left and bottom:
    return distance((x1, y1), (x2b, y2b))
  elif bottom and right:
    return distance((x1b, y1), (x2, y2b))
  elif right and top:
    return distance((x1b, y1b), (x2, y2))
  elif left:
    return x1 - x2b
  elif right:
    return x2 - x1b
  elif bottom:
    return y1 - y2b
  elif top:
    return y2 - y1b
  else: # rectangles intersect
    return 0.

def normalize(image):
  norm_image = image.copy()
  _, norm_image = cv2.threshold(norm_image, 127, 255, cv2.THRESH_TOZERO);
  return norm_image

def frame_sum(frame):
  s = np.count_nonzero(normalize(frame))
  return s

def frame_diff(a, b):
  diff = cv2.subtract(a, b)
  return (frame_sum(diff), diff)

# def merge_to_bbox(boxes):
#   # if there are no boxes, return an empty list
#   if len(boxes) == 0:
#     return []

#   # if the bounding boxes integers, convert them to floats --
#   # this is important since we'll be doing a bunch of divisions
#   if boxes.dtype.kind == "i":
#     boxes = boxes.astype("float")

#   # grab the coordinates of the bounding boxes
#   x1 = boxes[:,0]
#   y1 = boxes[:,1]
#   x2 = boxes[:,2]
#   y2 = boxes[:,3]

#   xx1 = np.min(x1) - 85
#   yy1 = np.min(y1) - 5
#   xx2 = np.max(x2) + 85
#   yy2 = np.max(y2) + 5

#   return (xx1, yy1, xx2, yy2)

