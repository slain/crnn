import numpy as np
import time
import cv2
import torch
from torch.autograd import Variable
import lib.utils.utils as utils
import lib.models.crnn as crnn
import lib.config.alphabets as alphabets
import yaml
from easydict import EasyDict as edict


class Recognition(object):  # pylint: disable=too-few-public-methods
  def __init__(self):
      with open('lib/config/360CC_config.yaml', 'r') as f:
          self.config = yaml.safe_load(f)
          self.config = edict(self.config)

      self.config.DATASET.ALPHABETS = alphabets.alphabet
      self.config.MODEL.NUM_CLASSES = len(self.config.DATASET.ALPHABETS)

      self.device = torch.device(
          'cuda:0') if torch.cuda.is_available() else torch.device('cpu')

      self.model = crnn.get_crnn(self.config).to(self.device)

      self.checkpoint = torch.load(
          'output/checkpoints/checkpoint_17_acc_0.9852.pth', map_location=torch.device("cpu"))
      if 'state_dict' in self.checkpoint.keys():
          self.model.load_state_dict(self.checkpoint['state_dict'])
      else:
          self.model.load_state_dict(self.checkpoint)

      self.converter = utils.strLabelConverter(self.config.DATASET.ALPHABETS)

  def __call__(self, rect):
      (t, img) = rect

      # cv2.imshow('img', img)
      # cv2.waitKey(0)

      # img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

      # github issues: https://github.com/Sierkinhane/CRNN_Chinese_Characters_Rec/issues/211
      h, w = img.shape

      if h <= 0 or w <= 0:
         return (t, '')

      # fisrt step: resize the height and width of image to (32, x)
      img = cv2.resize(img, (0, 0), fx=self.config.MODEL.IMAGE_SIZE.H / h,
                       fy=self.config.MODEL.IMAGE_SIZE.H / h, interpolation=cv2.INTER_CUBIC)
      # cv2.imshow('img', img)
      # cv2.waitKey(0)
      # second step: keep the ratio of image's text same with training
      h, w = img.shape
      w_cur = int(
          img.shape[1] / (self.config.MODEL.IMAGE_SIZE.OW / self.config.MODEL.IMAGE_SIZE.W))
      img = cv2.resize(img, (0, 0), fx=w_cur / w, fy=1.0,
                      interpolation=cv2.INTER_CUBIC)
      # cv2.imshow('img', img)
      # cv2.waitKey(0)
      img = np.reshape(img, (self.config.MODEL.IMAGE_SIZE.H, w_cur, 1))

      # normalize
      img = img.astype(np.float32)
      img = (img / 255. - self.config.DATASET.MEAN) / self.config.DATASET.STD
      img = img.transpose([2, 0, 1])
      img = torch.from_numpy(img)

      img = img.to(self.device)
      img = img.view(1, *img.size())
      self.model.eval()
      preds = self.model(img)
      # print(preds.shape)
      _, preds = preds.max(2)
      preds = preds.transpose(1, 0).contiguous().view(-1)

      preds_size = Variable(torch.IntTensor([preds.size(0)]))
      # print(preds)
      sim_pred = self.converter.decode(preds.data, preds_size.data, raw=False)

      # print('results: {0}'.format(sim_pred))
      return (t, sim_pred)
