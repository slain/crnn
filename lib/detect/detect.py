import multiprocessing
import cv2
import numpy as np
import re

from lib.detect.extractor import E1, E2, E3, E4, E5, B1, B2, B0, dilate, find_components, find_optimal_components_subset
from lib.detect.utils import rect_distance, distance, normalize, frame_diff, frame_sum, timecode, timecode_file, best_text
# from lib.detect.tesseract import Tesseract
from lib.detect.recognize import Recognition

def detect():
  video_path = 'samples/103.mp4'
  subtitles_path = 'samples/103.srt'

  recog = Recognition()
  pool = multiprocessing.Pool(8)

  model = E3()
  model2 = B0()

  f = open(subtitles_path, "w")

  try:
    cap = cv2.VideoCapture(video_path)
    fps = cap.get(cv2.CAP_PROP_FPS)
    cap.set(cv2.CAP_PROP_POS_FRAMES, 1*fps*100)

    sub_index = 0
    sub_start = 0
    sub_end = 0
    scene_detect = False
    next_detect = False
    sub_detect = False
    prev_rect = (0, 0, 0, 0)
    prev_rect_dist = False
    rects = []
    candidates = []
    last = False
    last_pos = 0

    while (True):
      if cv2.waitKey(25) & 0xFF == ord('q'):
        break

      if sub_index > 15:
        break

      ok, current = cap.read()
      if not ok:
        break

      model.extract(current)
      model2.extract(current)

      cleaned = dilate(model.cleaned, 12)

      pos = cap.get(cv2.CAP_PROP_POS_FRAMES)

      # cap.set(cv2.CAP_PROP_POS_FRAMES, pos + 2)
      t = pos / fps

      contours = find_components(cleaned)
      if len(contours) > 0:
        (x1, y1, x2, y2) = find_optimal_components_subset(contours, cleaned)
        x2 -= 5
        y2 -= 5
        if last is not False:
          try:
            (s, diff) = frame_diff(model.cleaned[y1:y2, x1:x2], last[y1:y2, x1:x2])
            if s >= 1000:
              next_detect = True
            last = model.cleaned
            # print(s, frame_sum(model.cleaned[y1:y2, x1:x2]))

            # cv2.imshow('frame', normalize(model.cleaned[y1:y2, x1:x2]))
            # cv2.imshow('diff', diff)
            # cv2.waitKey(0)
          except:
            last = model.cleaned
            # next_detect = True
            # print('ex next')
        else:
          last = model.cleaned

        if y2-y1 >= 35 and x2-x1 >= 50 and not next_detect and frame_sum(model.cleaned[y1:y2, x1:x2]) >= 100:
          scene_detect = True
          if sub_detect is False:
            sub_start = int(t * 1000) - 40
            sub_index += 1
            sub_detect = True
            print("Detect: [{}] {} --> ".format(sub_index, timecode(sub_start)), end="", flush=True)
            # if len(rects) < 25:
          else: 
            last_pos = pos

          if len(rects) <= 8:
            rects.append((t, 255-model.cleaned[y1-10:y2+5, x1-10:x2+5]))
            rects.append((t, 255-model2.cleaned[y1-10:y2+5, x1-10:x2+5]))
            rects.append((t, 255-model.cleaned[y1:y2, x1:x2]))
            rects.append((t, 255-model2.cleaned[y1:y2, x1:x2]))

          # cv2.rectangle(model.cropped, (x1, y1), (x2, y2), (0, 255, 0), 1)
          # cv2.imshow('frame', model.cropped)
          # cv2.waitKey(0)
        else:
          scene_detect = False

      if next_detect:
        next_detect = False
        scene_detect = False

      if scene_detect is False and sub_detect is True:
        sub_end = int((last_pos / fps) * 1000) + 40
        sub_detect = False
        print("{} => OK".format(timecode(sub_end)), end="\n", flush=True)
        # imagem = 255-rects[0][1]
        # cv2.imwrite(
        #     "../CRNN_Chinese_Characters_Rec/data/{}_{}.jpg".format(timecode_file(sub_start), timecode_file(sub_end)), imagem)
        candidates.append((sub_index, sub_start, sub_end, rects))
        rects = []

    print('\n...\n')

    for (sub_index, sub_start, sub_end, rects) in candidates:
      sequences = []
      for i, (t, text) in enumerate(pool.imap(recog, rects)):
        if len(text) < 1:
          continue
        sequences.append(text)
      # print(sequences)

      text = best_text(sequences)
      if len(text) < 1:
        continue

      print("Recognize: [{}] {} --> {}".format(sub_index, timecode(sub_start), timecode(sub_end)), end="\n", flush=True)
      print("{}".format(text), end="\n", flush=True)

      f.write("{}\n".format(sub_index))
      f.write("{} --> {}\n".format(timecode(sub_start), timecode(sub_end)))
      f.write(text + "\n\n")
      f.flush()

    print("Subtitles file created at {}".format(subtitles_path))
  except KeyboardInterrupt:
    return 1
  return 0

